// grab the mongoose module
var mongoose = require('mongoose');

// define our nerd model
// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Nerd', {
	userName : {type : String, default: ''},
	password : {type:String,default:''},
	token : {type:String,default:''},
	status : {type:String,default:''}
});
