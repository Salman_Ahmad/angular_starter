angular.module('MainCtrl', []).controller('MainController', function($scope,$state,$rootScope) {
	$scope.varVal=true;
    var localObject={};
    if(localStorage.getItem('newToken')){
        localObject=JSON.parse(localStorage.getItem('newToken'));
        if(localObject.data[0].status=='Admin') {
            $scope.varAdmin=true;
        }
        else{
           $scope.varUser=true;
        }
        $scope.varVal=false;
    }
    else{
        $state.go('login');
    }
     $scope.logout=function()
	{
		localStorage.clear();
        console.log(localStorage.getItem('newToken'));
		$state.go('login');
	}
	$scope.tagline = 'To the moon and back!';

});