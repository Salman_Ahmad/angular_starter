angular.module('LoginCtrl', []).controller('LoginController', function($scope,$http,$state) {

    $scope.tagline = 'Login Page';
    $scope.newStatus='';
    $scope.user={};
    $scope.checkUser=function(){
        $http.post('/checkUser',$scope.user,{}).then(function(result){
                if(result.status==200){
                    console.log(result);
                    localStorage.setItem('newToken',JSON.stringify(result));
                    $state.go('geeks');
                }
            },function(err)
        {
            console.log('in error');
            $state.go('login');
            window.alert('Wrong password or username');
            $scope.user={};
        });
    };

});
