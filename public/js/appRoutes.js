angular.module('appRoutes', []).config(['$stateProvider','$locationProvider', function($stateProvider,$locationProvider) {

    $stateProvider

		// home page
        .state('home', {
            url:'/',
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })

		.state('nerds', {
			url:'/nerds',
			templateUrl: 'views/nerd.html',
			controller: 'NerdController'
		})

		.state('geeks', {
			url:'/geeks',
			templateUrl: 'views/geek.html',
			controller: 'GeekController'	
		})
		.state('admin',{
    	url:'/admin',
		templateUrl:'views/admin.html',
		controller:'AdminController'
		})
        .state('user',{
            url:'/user',
            templateUrl:'views/user.html',
            controller:'UserController'
        })
		.state('login',{
			url:'/login',
			templateUrl:'views/login.html',
			controller:'LoginController'
		});
    $locationProvider.html5Mode(true);
}]);